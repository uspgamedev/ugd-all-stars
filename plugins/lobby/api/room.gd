extends Resource
class_name LobbyRoom

var room_name := ""
var room_seed := 0
var player_infos := {} # multiplayer_id -> player info
var local_id := 0

func _init(room_name_ := "", player_infos_ := {}, local_id_ := 0):
	room_name = room_name_
	player_infos = player_infos_
	local_id = local_id_

func has_player_id(id: int) -> bool:
	return player_infos.has(id)
	
func add_player_info(info: Dictionary):
	player_infos[info.id] = info

func remove_player_info(id: int):
	player_infos.erase(id)

func get_local_info():
	return player_infos[local_id]

func get_num_players() -> int:
	return player_infos.size()

func get_infos_ordered() -> Array:
	var keys = player_infos.keys()
	keys.sort_custom(func(x, y): return x < y)
	return keys.map(func(key): return player_infos[key])
