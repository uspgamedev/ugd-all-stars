extends Node

var server := false

@export var client_api_scene: PackedScene
@export_file("*.tscn") var server_scene_path := "res://plugins/lobby/example/server.tscn"

@export var lobby_config: LobbyConfig = LobbyConfig.new()

@onready var join_button: Button = %JoinRoom
@onready var ready_button: Button = %Ready
@onready var room_name_label: Label = %RoomName
@onready var room_name_field: TextEdit = %Room
@onready var entries: RichTextLabel = %Entries
@onready var name_field: TextEdit = %Name
@onready var color_1_field: ColorPickerButton = %Color1
@onready var color_2_field: ColorPickerButton = %Color2

var lobby_api: LobbyAPI

var joined := false
var server_pid = null

func _ready():
	lobby_api = client_api_scene.instantiate()
	lobby_api.config = lobby_config
	
	get_tree().root.add_child.bind(lobby_api).call_deferred()
	
	lobby_api.room_updated.connect(update_room_info)
	lobby_api.game_started.connect(start_game)
	

func create_server():
	server_pid = OS.create_instance([server_scene_path])

func close_server():
	if server_pid != null: 
		OS.kill(server_pid)
		server_pid = null

func get_player_info() -> Dictionary:
	return {
		"name": name_field.text,
		"color_1": color_1_field.color.to_html(),
		"color_2": color_2_field.color.to_html()
	}

func update_room_info(room: LobbyRoom):
	room_name_label.text = room.room_name + " "
	entries.text = ""
	for info in room.player_infos.values():
		entries.text += "[color=%s] %s[/color]\n" % [Color.GREEN.to_html() if info.ready else Color.RED.to_html(), info.name]
	
func start_game(room: LobbyRoom):
	for info in room.player_infos.values():
		print(info.name, " info:\n", info)
	queue_free()

func _on_join_room_pressed():
	if not joined:
		room_name_field.editable = false
		join_button.text = "Leave room"
		lobby_api.enter_room(room_name_field.text, get_player_info())
		joined = true
	else:
		joined = false
		join_button.text = "Join room"
		room_name_field.editable = true
		lobby_api.leave_room()
		room_name_label.text = "Room"
		

func _on_ready_pressed():
	if joined:
		ready_button.disabled = true
		lobby_api.set_ready(true)


func _on_server_pressed():
	create_server()
