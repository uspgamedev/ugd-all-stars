extends Node
class_name LobbyClient

var client_socket := ENetMultiplayerPeer.new()

@export var is_server := false
@onready var server: LobbyServer

var current_player_info := {}
var room: LobbyRoom = null

enum Connection {
	DISCONNECTED,
	CONNECTING,
	CONNECTED,
	
}

var active := Connection.DISCONNECTED

# ------------------------------------------------------------------
# |                              API                               |
# ------------------------------------------------------------------

func enter_room(room_name: String, player_info: Dictionary, config: LobbyConfig):
	room = LobbyRoom.new(room_name)
	current_player_info = player_info
	connect_to_server(config)
	try_connect(config)

func try_connect(config: LobbyConfig):
	if multiplayer.multiplayer_peer.get_connection_status() == MultiplayerPeer.CONNECTION_DISCONNECTED:
		connect_to_server(config)
	if multiplayer.multiplayer_peer.get_connection_status() != MultiplayerPeer.CONNECTION_CONNECTED:
		get_tree().create_timer(0.5).timeout.connect(try_connect.bind(config))
				
	
func change_info(player_info: Dictionary):
	if room == null:
		return
	
	server.change_info.rpc_id(1, room.room_name, player_info)

func leave_room():
	if room != null:
		client_socket.close()
		room = null

func set_ready(value: bool):
	if room == null:
		return
	
	server.player_ready.rpc_id(1, room.room_name, value)

# ------------------------------------------------------------------
# |                            PRIVATE                             |
# ------------------------------------------------------------------

func _ready():
	multiplayer.peer_connected.connect(receive_peer)

func init_client(is_server_: bool, server_: LobbyServer):
	is_server = is_server_
	server = server_
	
var config2: LobbyConfig

func connect_to_server(config: LobbyConfig):
	disconnect_to_server()
	active = Connection.CONNECTING
	config2 = config
	client_socket = ENetMultiplayerPeer.new()
	
	if client_socket.create_client(config.server_address, config.server_port) != OK:
		push_error("Cannot connect to server")
		client_socket = null
		return
		
	if config.use_dtls:
		if not ResourceLoader.exists(config.certificate_file):
			push_error("Invalid Certificate Path")
			get_tree().quit()
			return
		
		var cert = load(config.certificate_file)
		var tls_options := TLSOptions.client(cert) if config.validate_certificate else TLSOptions.client_unsafe(cert)
		
		var setup_result := client_socket.host.dtls_client_setup(config.server_address, tls_options)
		
		for i in 100:
			if setup_result == OK:
				active = Connection.CONNECTED
				break
			elif i == 1:
				push_error("Cannot setup DTLS")
				active = Connection.DISCONNECTED
				return
			
			setup_result = client_socket.host.dtls_client_setup(config.server_address, tls_options)
		
	#client_socket.compress(ENetConnection.COMPRESS_RANGE_CODER)
	multiplayer.set_multiplayer_peer(client_socket)	

func disconnect_to_server():
	active = Connection.DISCONNECTED
	multiplayer.multiplayer_peer = null
	if client_socket != null:
		client_socket.close()
	
func receive_peer(id: int):
	if not is_server and id == 1:
		entered_room()

func entered_room():
	current_player_info.id = client_socket.get_unique_id()
	current_player_info.ready = false
	server.join_room.rpc_id(1, room.room_name, current_player_info)
	
func _exit_tree():
	if is_instance_valid(client_socket.host):
		client_socket.disconnect_peer(1, true)
		client_socket.close()


# ------------------------------------------------------------------
# |                              RPCS                              |
# ------------------------------------------------------------------

signal room_updated(room: LobbyRoom)
signal game_started(room: LobbyRoom)

@rpc("any_peer", "call_remote", "reliable")
func update_room(room_name: String, player_infos: Dictionary):
	room = LobbyRoom.new(room_name, player_infos, multiplayer.get_unique_id())
	room_updated.emit(room)

@rpc("any_peer", "call_local", "reliable")
func start_game(player_infos: Dictionary, room_seed):
	room.player_infos = player_infos
	room.room_seed = room_seed
	
	var ids = room.player_infos.keys()
	
	for peer_id in multiplayer.get_peers():
		if not ids.has(peer_id) and peer_id != 1:
			multiplayer.multiplayer_peer.disconnect_peer(peer_id)
	
	game_started.emit(room)

# ------------------------------------------------------------------
