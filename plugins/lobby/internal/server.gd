extends Node
class_name LobbyServer
const MAX_USERS = 100

var is_authority := false

@onready var client: LobbyClient

var server_socket := ENetMultiplayerPeer.new()

signal client_connected(client_id: int)
signal client_disconnected(client_id: int)

func _ready():
	multiplayer.peer_connected.connect(func(x): client_connected.emit(x))
	multiplayer.peer_disconnected.connect(func(x): client_disconnected.emit(x))
	multiplayer.peer_disconnected.connect(remove_peer)
	multiplayer.peer_connected.connect(func(x): print("Connected: ", x))
	multiplayer.peer_disconnected.connect(func(x): print("Disconnected: ", x))


func init_server(is_authority_: bool, client_: LobbyClient, config: LobbyConfig):
	is_authority = is_authority_
	client = client_
	
	if is_authority:
		listen(config)

var active := false
var config2

func listen(config: LobbyConfig): 
	config2 = config
	server_socket = ENetMultiplayerPeer.new()
	
	if server_socket.create_server(config.server_port, MAX_USERS):
		push_error("Cannot create server")
		return
	
	if config.use_dtls:
		if not ResourceLoader.exists(config.server_private_key_file) or not ResourceLoader.exists(config.certificate_file):
			push_error("Invalid Key or Certificate Path")
			get_tree().quit()
			return
		
		var key = load(config.server_private_key_file)
		var cert = load(config.certificate_file)
		
		var tls_options := TLSOptions.server(key, cert)

		server_socket.host.dtls_server_setup(tls_options)
		
	multiplayer.set_multiplayer_peer(server_socket)
	active = true
	
func remove_peer(id: int):
	if is_authority:
		for room_name in rooms.keys():
			if rooms[room_name].player_infos.has(id):
				leave_room(room_name, id)
				server_socket.disconnect_peer(id)

func _process(_delta):
	if is_authority and active and \
		multiplayer.multiplayer_peer.get_connection_status() == MultiplayerPeer.CONNECTION_DISCONNECTED:
		listen(config2)
	
	if multiplayer.multiplayer_peer.get_connection_status() != MultiplayerPeer.CONNECTION_CONNECTED:
		rooms.clear()

# ------------------------------------------------------------------
# |                           SERVER  RPCS                         |
# ------------------------------------------------------------------

var rooms := {}

@rpc("any_peer", "call_remote", "reliable")
func join_room(room_name: String, player_info: Dictionary):
	var room: LobbyRoom
	
	if not rooms.has(room_name):
		room = LobbyRoom.new(room_name)
		rooms[room_name] = room
		room.room_seed = randi()
	else:
		room = rooms[room_name]
	
	player_info.id = multiplayer.get_remote_sender_id()
	
	if room.has_player_id(player_info.id):
		# TODO: Send error to client
		return
		
	room.add_player_info(player_info)
	
	for info in room.player_infos.values():
		client.update_room.rpc_id(info.id, room_name, room.player_infos)

@rpc("any_peer", "call_remote", "reliable")
func player_ready(room_name: String, is_ready := true):
	var sender_id = multiplayer.get_remote_sender_id()
	var room: LobbyRoom = rooms.get(room_name, null)
	
	if room == null or not room.player_infos.has(sender_id):
		# TODO: Send error to client
		return
		
	room.player_infos[sender_id].ready = is_ready
	change_info(room_name, room.player_infos[sender_id])
	
@rpc("any_peer", "call_remote", "reliable")
func change_info(room_name: String, player_info: Dictionary):
	var sender_id = multiplayer.get_remote_sender_id()
	var room: LobbyRoom = rooms.get(room_name, null)
	
	if room == null or not room.player_infos.has(sender_id):
		# TODO: Send error to client
		return

	player_info.id = sender_id
	if not player_info.has("ready"):
		player_info.ready = room.player_infos[sender_id].ready
	room.player_infos[sender_id] = player_info
	
	for peer_id in room.player_infos.keys():
		if multiplayer.get_peers().find(peer_id) == -1:
			room.player_infos.erase(peer_id)
	
	for info in room.player_infos.values():
		client.update_room.rpc_id(info.id, room_name, room.player_infos)
	
	if room.player_infos.values().filter(func(x): return not x.ready).size() > 0:
		return
		
	for info in room.player_infos.values():
		client.start_game.rpc_id(info.id, room.player_infos, room.room_seed)
	
	rooms.erase(room_name)


#@rpc("any_peer", "call_remote", "reliable")
func leave_room(room_name: String, id = null):
	var room: LobbyRoom = rooms.get(room_name, null)
	
	if multiplayer.get_remote_sender_id() == 1:
		id = multiplayer.get_remote_sender_id()
	
	if not rooms.has(room_name) or not room.player_infos.has(id):
		# TODO: Send error to client
		return
		
	if not room.has_player_id(id):
		# TODO: handle error
		return
		
	room.remove_player_info(id)
	
	for key in room.player_infos.keys():
		room.player_infos[key].ready = false
	
		
	if room.get_num_players() == 0:
		rooms.erase(room_name)
	else:
		for info in room.player_infos.values():
			client.update_room.rpc_id(info.id, room_name, room.player_infos)
