extends Resource
class_name InputCommand

@export var name := ""

func get_value(_id := -1) -> Variant:
	push_error("Virtual method")
	return null

func get_value_from_map(input_map: Dictionary) -> Variant: 
	return input_map.get(name)
