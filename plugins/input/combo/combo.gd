extends InputCommand
class_name Combo

func check_combo(inputs: Array[Dictionary]) -> bool:
	return _check_combo(inputs)

func _check_combo(_inputs: Array[Dictionary]) -> bool:
	push_error("Virtual method")
	return false
