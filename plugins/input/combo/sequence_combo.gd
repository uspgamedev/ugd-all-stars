#@tool
extends Combo
class_name SequenceCombo

@export var inputs: Array[InputCommand]

## Example: "A(1-5)B(1-10)C(3-4)" -> 1 to 5 frames of A then 1 to 10 frames of B then 3 to 4 frames of C
@export var sequence := "" : set = set_sequence

var machine_head := SequenceState.new() 

func set_sequence(s):
	sequence = s
	build_machine()

class SequenceState:
	var is_final := false
	var transitions := {}
	
	func show():
		if is_final:
			printraw("O --> ")
		else:
			printraw("--> ")
		for t in transitions:
			printraw(t, ", ")
		print()
		for t in transitions.values():
			t.show()
	
func build_machine():
	var seq := sequence.replace("(", "|").replace(")", "|").replace("-", "|").split("|") as Array
	
	seq = seq.map(func(x): return x.strip_edges()).filter(func(x): return not x.strip_edges().is_empty())
	
	machine_head = SequenceState.new() 
	var prev_states: Array = [machine_head]
	
	while !seq.is_empty():
		var higher = seq.pop_back().to_int()
		var lower = seq.pop_back().to_int()
		var input_idx = seq.pop_back().unicode_at(0) - "A".unicode_at(0)
		#print(input_idx, " ", lower, " ", higher)
		
		var states = []
		
		for i in higher:
			var state := SequenceState.new()
			
			for prev_state in prev_states:
				prev_state.transitions[input_idx] = state
				
			prev_states = [state]
			
			if i+1 >= lower:
				states.push_back(state)
		
		prev_states = states
	
	for state in prev_states:
		state.is_final = true
		

func _check_combo(input_maps: Array[Dictionary]) -> bool:
	input_maps = input_maps.duplicate()
	#print(input_maps.map(func(x): return x.frame))
	
	var state = machine_head
	
	while not (input_maps.is_empty() or state.is_final or state.transitions.is_empty()):
		var input_map = input_maps.pop_back()
		var found := false
		
		for transition in state.transitions.keys():
			if inputs[transition].get_value_from_map(input_map) == true:
					state = state.transitions[transition]
					found = true
					break
		
		#if found:
			#print(input_map.frame, " ", state.is_final)
			
		if not found:
			break

	if state.is_final:
		return true
	
	return false

## sequence = "A (1-3) B (2-3)"
func tests():
	var prev = [
		{"jump": false, "walk": false},
		{"jump": false, "walk": false},
		{"jump": false, "walk": false},
		{"jump": false, "walk": false},
		{"jump": false, "walk": false},
	]
	
	assert(check_combo(prev + [
		{"jump": true, "walk": false},
		{"jump": false, "walk": true},
		{"jump": false, "walk": true},
	]))
	assert(not check_combo(prev + [
		{"jump": true, "walk": false},
		{"jump": false, "walk": true},
		{"jump": false, "walk": false},
	]))
	assert(check_combo(prev + [
		{"jump": true, "walk": false},
		{"jump": true, "walk": false},
		{"jump": false, "walk": true},
		{"jump": false, "walk": true},
		{"jump": false, "walk": true},
	]))
