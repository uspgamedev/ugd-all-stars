extends InputCommand
class_name BoolInput

func _init(action: String = ""):
	name = action

func is_pressed(id: int = -1) -> bool:
	if id == -1:
		return Input.is_action_pressed(name)	
		
	return Input.is_action_pressed(name + "_" + str(id))	

func get_value(id: int = -1) -> Variant:
	return is_pressed(id)
