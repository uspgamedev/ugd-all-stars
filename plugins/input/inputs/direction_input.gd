extends BoolInput
class_name DirectionInput

enum Directions { NONE, L, LU, U, RU, R, RD, D, LD }

@export var directional_input: DirectionalInput
@export var direction := Directions.NONE


func _init(directional_input_: DirectionalInput = null, direction_: Directions = Directions.NONE):
	directional_input = directional_input_
	direction = direction_

func is_pressed(id: int = -1) -> bool:
	return check_direction(directional_input.get_direction(id))

func check_direction(dir: Vector2) -> bool:
	var dir2 := get_direction()
	
	if dir.is_zero_approx() and dir2.is_zero_approx():
		return true
		
	if dir.is_zero_approx() != dir2.is_zero_approx():
		return false
		
	return abs(dir.angle_to(dir2)) < PI/8

func get_value_from_map(input_map: Dictionary) -> Variant: 
	return check_direction(directional_input.get_value_from_map(input_map))

func get_direction() -> Vector2:
	match direction:
		Directions.NONE: 
			return Vector2()
	return Vector2.LEFT.rotated((direction-1)*PI/4)
	
