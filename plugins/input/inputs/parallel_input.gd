extends BoolInput
class_name ParallelInput

@export var inputs: Array[BoolInput] = []

func is_pressed(id: int = -1) -> bool:
	for input in inputs:
		if not input.is_pressed(id):
			return false
	return true

func get_value_from_map(input_map: Dictionary) -> Variant: 
	for input in inputs:
		if not input.get_value_from_map(input_map):
			return false
	return true

	
