extends InputCommand
class_name DirectionalInput

func _init(prefix: String = ""):
	name = prefix

func get_direction(id := -1) -> Vector2:
	var direction = Vector2()
	
	if id == -1:
		direction = Input.get_vector(name + "_left", name + "_right", name + "_up", name + "_down")
	else:
		direction = Input.get_vector(
		name + "_left_" + str(id),
		name + "_right_" + str(id),
		name + "_up_" + str(id),
		name + "_down_" + str(id)
	)
	
	if direction.is_zero_approx():
		return Vector2()
	return direction.normalized()
	


func get_value(id := -1) -> Variant:
	return get_direction(id)

