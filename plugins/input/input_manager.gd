extends Node
class_name InputManager

const MAX_FRAMES = 20

@export var input_type: InputType

var input_map: Dictionary
var inputs: Array[InputCommand]

var input_buffer: InputBuffer = SimpleBuffer.new()
var input_queue: Array[Dictionary] = []
	
func _ready():
	input_map = input_type.get_prototype()
	inputs = input_type.get_inputs()

	input_buffer.empty_input = serialize()
	for _i in MAX_FRAMES:
		input_queue.push_back(serialize())

var frame = 0

func _process(_delta):
	frame += 1
	if is_multiplayer_authority():
		var id = get_id()
		for input in inputs:
			input_map[input.name] = input.get_value(id)
		
		sync.rpc(serialize())
		input_queue.push_back(serialize())
		input_queue.pop_front()
		#ping.rpc(Time.get_ticks_msec())
			
	else:
		deserialize(input_buffer.pop_input())

func is_pressed(input: BoolInput) -> bool:
	if not is_multiplayer_authority():
		return false
		
	if input != null and input_map.has(input.name):
		return input_map[input.name]
		
	push_error("Input not found: ", input)
	return false

func get_direction(input: DirectionalInput = null) -> Vector2:
	if not is_multiplayer_authority():
		return Vector2()
	
	if input == null:
		input = input_type._get_directional_inputs().front()
	
	if input != null and input_map.has(input.name):
		return input_map[input.name]
	
	push_error("Input not found: ", input)
	return Vector2()

func is_just_pressed(input: BoolInput) -> bool:
	return input.get_value_from_map(input_queue[-1]) == true and \
		input.get_value_from_map(input_queue[-2]) == false


func or_is_just_pressed(input_array: Array[BoolInput]) -> bool:
	for input in input_array:
		if input.get_value_from_map(input_queue[-1]) == true and \
			input.get_value_from_map(input_queue[-2]) == false:
			return true
	return false
		
		
func check_combo(combo: Combo) -> bool:
	return combo.check_combo(input_queue)
	
func serialize() -> Dictionary:
	var dict := {}
	var id = get_id()
	for input in inputs:
		dict[input.name] = input.get_value(id)
	dict.frame = frame
		
	return dict

func deserialize(serialized_input: Dictionary):
	for input in input_map.keys():
		input_map[input] = serialized_input[input]

func get_id() -> int:
	return Entity.get_entity(self).binding_id

@rpc("authority", "call_remote", "unreliable_ordered")
func sync(serialized_input: Dictionary):
	#last_input = serialized_input
	input_buffer.push_input(serialized_input)
	
@rpc("any_peer", "call_remote", "unreliable")
func ping(time):
	pong.rpc_id(multiplayer.get_remote_sender_id(), time)

@rpc("any_peer", "call_remote", "unreliable")
func pong(time):
	print(multiplayer.get_remote_sender_id(),  ": ", Time.get_ticks_msec() - time)

