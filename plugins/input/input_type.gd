extends Resource
class_name InputType

# Returns enum value -> action name 
func _get_bool_inputs() -> Array[BoolInput]:
	push_error("Virtual method")
	return []
	
# Returns enum value -> action name 
func _get_directional_inputs() -> Array[DirectionalInput]:
	push_error("Virtual method")
	return []

func get_inputs() -> Array[InputCommand]:
	var ret: Array[InputCommand] = []
	
	for input in _get_bool_inputs():
		ret.push_back(input)
		
	for input in _get_directional_inputs():
		ret.push_back(input)
	
	return ret

func get_prototype() -> Dictionary:
	var input_dict := {}
	var bool_inputs := _get_bool_inputs()
	var directional_inputs := _get_directional_inputs()
	
	for action in bool_inputs:
		input_dict[action.name] = false
		
	for action in directional_inputs:
		input_dict[action.name] = Vector2()
	
	return input_dict
	
