extends InputBuffer
class_name SimpleBuffer

var buffer = null

func push_input(input: Dictionary):
	buffer = input
	
func pop_input() -> Dictionary:
	if buffer == null:
		return empty_input
	return buffer
