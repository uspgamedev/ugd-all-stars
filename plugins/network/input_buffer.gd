extends Resource
class_name InputBuffer

var empty_input: Dictionary = {}

func push_input(_input: Dictionary):
	push_error("Virtual method")
	return

func pop_input() -> Dictionary:
	push_error("Virtual method")
	return {}
