extends InputBuffer
class_name LockstepBuffer

var buffer

func push_input(input: Dictionary):
	buffer = input
	
func pop_input() -> Dictionary:
	return buffer
