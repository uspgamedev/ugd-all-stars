extends Node
class_name StateMachine


@onready var body: Body = Entity.get_entity(self).get_component(Body)
@onready var player_input: InputManager = Entity.get_entity(self).get_component(InputManager)

var current_state: State = null : set = set_current_state
var previous_state: State = null
var air_jumps: int


func _ready():
	set_current_state.bind(get_child(0)).call_deferred()
	air_jumps = body.AIR_JUMPS


func state_logic(delta):
	body._physics_process(delta)


func _physics_process(delta):
	if current_state != null:
		state_logic(delta)
		body.current_state.text = str(current_state.get_path()).split("/")[-1]
		current_state._apply_input(player_input)
		current_state.update(delta)

	
func state_includes(state_array) -> bool:
	for each_state in state_array:
		if current_state == each_state:
			return true
	return false


func get_state(state_script: Script) -> State:
	for child in get_children():
		## TODO: get node that inherits from script
		if child.get_script() == state_script:
			return child
	push_error("no state " + str(state_script) + " in state machine")
	return null


func set_current_state(new_state: State):
	previous_state = current_state
	current_state = new_state
	
	if previous_state != current_state:
		if previous_state != null:
			previous_state.exit_state()
		if new_state != null:
			current_state.enter_state()
