extends Node
class_name State


var frame = 0
@onready var body: Body = Entity.get_entity(self).get_component(Body) 
@onready var state_machine: StateMachine = get_parent()

signal entered_state(state: State)



func update(delta):
	frame += 1
	self._update(delta)
	

## get state given a script
func get_state(state_script: Script) -> State:
	for child in get_parent().get_children():
		## TODO: get node that inherits from script
		if child.get_script() == state_script:
			return child
	push_error("no state " + str(state_script) + " in state machine")
	return null


#func try_change_state_to(state_script: Script) -> bool:
	#return get_state(state_script)._try_change_state()


## enters the state this function is written into
func enter_state():
	entered_state.emit(self)
	frame = 0
	_enter_state()


## exits the state this function is written into
func exit_state():
	_exit_state()
	

# We have set_state at home (set_current_state in class StateMachine)
#func set_state(state_script: Script):
	#var state_machine: StateMachine = get_parent()
	#state_machine.set_current_state(get_state(state_script))


func get_input() -> InputManager:
	return get_parent().player_input


#func get_state_machine() -> StateMachine:
	#return get_parent()


## applies input to change from this state to another one
func apply_input(input: InputManager):
	_apply_input(input)


## tries to enter the state this function is written into given an input
func try_change_state(input: InputManager) -> bool:
	return _try_change_state(input)


## updates inside current state
func _update(_delta) -> State:
	return
	

# delegate body.reset_frame() to the enter_state method	
func _enter_state():
	push_error("no virtual method _enter_state in ", self)
	
	
# is it necessary?
func _exit_state():
	push_error("no virtual method _exit_state in ", self)


func _apply_input(_input: InputManager):
	push_error("no virtual method _apply_input in ", self)


func _try_change_state(_input: InputManager) -> bool:
	push_error("no virtual method _try_change_state() in ", self)
	return false
