extends Node

const BUFFER_SIZE := 10

@export var state_owner : Node
@export var sync : MultiplayerSynchronizer

@export var state: ObjectState

var buffer := []

var wait := true

func _ready():
	sync.synchronized.connect(_on_sync_synchronized)
	

func _physics_process(_delta):
	if sync.is_multiplayer_authority():
		state.update_state()
		return
		
	if  buffer.size() >= BUFFER_SIZE:
		wait = false
	if buffer.size() <= 0:
		wait = true
		
	if not wait and buffer.size() > 0:
		var state_buffer = buffer.pop_front()
		state_buffer.apply_state()
		state_buffer.queue_free()
		wait = false

func _on_sync_synchronized():
	if buffer.size() >= BUFFER_SIZE:
		buffer.pop_front()
	
	buffer.push_back(state.duplicate())
