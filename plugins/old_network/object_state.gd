extends Node
class_name ObjectState

func _ready():
	update_state()
		
func update_state():
	push_error("Virtual method")

func apply_state():
	push_error("Virtual method")
