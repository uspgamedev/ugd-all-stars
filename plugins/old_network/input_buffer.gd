extends Node

const BUFFER_SIZE := 10

@export var input_owner : Node
@export var sync : MultiplayerSynchronizer

@export var input: Node

var buffer := []

var wait := true

func _ready():
	sync.synchronized.connect(_on_sync_synchronized)
	if sync.is_multiplayer_authority():
		input_owner.input = input
	

func _physics_process(_delta):
	if sync.is_multiplayer_authority():
		return
		
	if  buffer.size() >= BUFFER_SIZE:
		wait = false
	if buffer.size() <= 0:
		wait = true
		
	if not wait and buffer.size() > 0:
		input_owner.input.free()
		input_owner.input = buffer.pop_front()
		
		wait = false

func _on_sync_synchronized():
	if buffer.size() >= BUFFER_SIZE:
		buffer.pop_front()
	
	buffer.push_back(input.duplicate())

