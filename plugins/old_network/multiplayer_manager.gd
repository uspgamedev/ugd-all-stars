extends Node

const MAX_PLAYERS := 16

var peers := {}

var peer = ENetMultiplayerPeer.new()

func _ready():
	multiplayer.peer_connected.connect(_on_peer_connected)
#	multiplayer.connection_failed
	multiplayer.peer_disconnected.connect(_on_peer_disconnected)
#	multiplayer.connected_to_server
#	multiplayer.server_disconnected
	
func _on_peer_connected(id: int):
	peers[id] = true
	
func _on_peer_disconnected(id: int):
	peers.erase(id)

func _init_server(port: int) -> bool:
	if peer.create_server(port, MAX_PLAYERS):
		return false
	peer.get_host().compress(ENetConnection.COMPRESS_RANGE_CODER)
	multiplayer.set_multiplayer_peer(peer)
	
	return true
	
func _init_client(ip: String, port: int) -> bool:
	if not ip.is_valid_ip_address() or peer.create_client(ip, port):
		return false
	
	peer.get_host().compress(ENetConnection.COMPRESS_RANGE_CODER)
	multiplayer.set_multiplayer_peer(peer)
	
	return true
