extends Node
class_name Entity

const ENTITY_GROUP := "ENTITY"

func _notification(what):
	match what:
		NOTIFICATION_READY:
			add_to_group(ENTITY_GROUP)

func get_component(component_type: Script) -> Node:
	for c in get_children():
		var child := c as Node
		if child.get_script() == component_type:
			return child

	return null

static func get_entity(component: Node) -> Entity:
	var parent = component.get_parent()

	while parent != null:
		if parent is Entity:
			return parent

		parent = parent.get_parent()

	return null

static func get_entities() -> Array:
	return Engine.get_main_loop().get_nodes_in_group(ENTITY_GROUP)
	
static func get_singleton(component: Script) -> Node:
	var entities = Engine.get_main_loop().get_nodes_in_group(ENTITY_GROUP) \
			.filter(func(x): return x.get_component(component) != null)
	if entities.size() == 1:
		return entities[0].get_component(component)
	else:
		return null
