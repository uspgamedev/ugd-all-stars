@tool
extends StaticBody2D
class_name WindowWall

@export_enum("ceiling", "right_wall", "floor", "left_wall") var orientation := 0

@export var length := 300.0 : set = set_length

@onready var direction = Vector2(0, -1).rotated(-orientation*PI/2.0) 
@onready var rect: MarginContainer = %Rect
@onready var shape: Shape2D = %Box.shape
@onready var shape2 = $Area2D/CollisionShape2D.shape

const DELTA = 40.0 
const EXPANSION_DELAY := 0.15

signal moved(orientation: int, delta: float)

func _ready():
	self.rotation = direction.angle_to(Vector2(0, -1))
	shape = %Box.shape 

var resize_tween: Tween = null

func resize(ending: int, delta):
	if shape is SegmentShape2D:
		resize_tween = create_tween()
		resize_tween.tween_property(self, "length", length + delta, EXPANSION_DELAY)
		if (orientation - ending + 4)%4 == 1:
			var s = -1 if (orientation == 0 or orientation == 2) else -1
			resize_tween.parallel().tween_property(self, "position", position + s*delta*direction.rotated(PI/2), EXPANSION_DELAY)
				
	else:
		push_error("Wrong Shape")	

var move_tween : Tween = null

func _on_area_2d_area_entered(area):
	if area is Attack and move_tween == null:
		var delta = DELTA * (Entity.get_entity(area).get_component(Body).global_position - self.global_position).normalized().dot(direction)
		var new_position = position - direction * delta
		move_tween = create_tween()
		move_tween.tween_property(self, "position", new_position, EXPANSION_DELAY)
		move_tween.finished.connect(func(): move_tween = null)
		moved.emit(orientation, delta)
		
func set_length(new_length: float):
	length = new_length
	(func(): 
		shape.b.x = new_length
		shape2.b.x = new_length
		rect.offset_right = new_length
	).call_deferred()
	
