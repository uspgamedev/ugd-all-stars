extends Node2D

var summoner: Entity

const DURATION = 10.0

var walls = {}

func _ready():
	for child in get_children():
		if child is WindowWall:
			walls[child.orientation] = child
			child.moved.connect(_on_wall_resize)
	
	await get_tree().create_timer(DURATION).timeout
	queue_free()


func _on_wall_resize(orientation: int, delta: float):
	if orientation%4%2 == 1:
		walls[(orientation+1)%4].resize(orientation, delta)
		walls[(orientation+3)%4].resize(orientation, delta)
	else:
		walls[(orientation+1)%4].resize(orientation, -delta)
		walls[(orientation+3)%4].resize(orientation, -delta)


func _enter_tree():
	pass
