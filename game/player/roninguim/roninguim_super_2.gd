extends SuperAttack

@export var approach_duration = 2.0
@export var duration := 1.0
@export var return_duration = 4.0

@export var size := Vector2(540, 360)

@onready var viewport_size := get_viewport().get_visible_rect().size
@onready var camera := get_viewport().get_camera_2d()
#@onready var center := camera.position

@onready var wall_crawling_sfx: AudioStreamPlayer = %WallCrawlingSFX
var speed = 0.

func activate(_from: Entity, _direction: Vector2, _target: Entity = null):
	World.get_world().add_child(self)
	return
	
func _ready():
	(func():
		var remote = RemoteTransform2D.new()
		camera.add_child(remote)
		remote.remote_path = remote.get_path_to(self)
		activate2()
		
	).call_deferred()

func activate2():
	var camera_zoom = camera.zoom
	#center /= camera_zoom
	size /= camera_zoom
	viewport_size /= camera_zoom
	
	wall_crawling_sfx.play()
	
	$Wall.position = -viewport_size/2.
	$Wall2.position = -viewport_size/2.
	$Wall3.position = viewport_size/2.
	$Wall4.position = viewport_size/2.
	
	speed = (viewport_size - size).length()/(2.*approach_duration)
	create_tween().tween_property($Wall, "position", - size/2., approach_duration)
	create_tween().tween_property($Wall2, "position", - size/2., approach_duration)
	create_tween().tween_property($Wall3, "position", + size/2., approach_duration)
	create_tween().tween_property($Wall4, "position", + size/2., approach_duration)
	#create_tween().tween_property(camera, "zoom", viewport_size.length()/size.length()*camera_zoom, approach_duration)
	create_tween().tween_method((func(x):
		camera.zoom = (viewport_size.length()/((1.-x)*viewport_size + x*size).length())*camera_zoom
	), 0., 1., approach_duration)
	
	
	get_tree().create_timer(approach_duration).timeout.connect(
		func():
			
			wall_crawling_sfx.stop()
			get_tree().create_timer(duration).timeout.connect(
				func():
					wall_crawling_sfx.play()
					create_tween().tween_property($Wall, "position", -viewport_size/2., return_duration)
					create_tween().tween_property($Wall2, "position", -viewport_size/2., return_duration)
					create_tween().tween_property($Wall3, "position", viewport_size/2., return_duration)
					create_tween().tween_property($Wall4, "position", viewport_size/2., return_duration)
					#create_tween().tween_property(camera, "zoom", camera_zoom, return_duration)
					create_tween().tween_method((func(x):
						camera.zoom = (viewport_size.length()/((1.-x)*viewport_size + x*size).length())*camera_zoom
					), 1., 0., return_duration)
					
					get_tree().create_timer(return_duration).timeout.connect(
						func():
							
							wall_crawling_sfx.stop()
							queue_free()
					)	
			)
	)
