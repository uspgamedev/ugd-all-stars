class_name Jumpsquat
extends State


var jumpsquat_frames = 3
	

func _apply_input(_input: InputManager):
	pass


func _update(_delta):
	if frame >= jumpsquat_frames:
		if get_input().is_pressed(PlayerInput.JUMP):
			state_machine.set_current_state(get_state(Fullhop))
		else:
			state_machine.set_current_state(get_state(Shorthop))		
	return self
	
	
func _try_change_state(input: InputManager) -> bool:
	if input.is_pressed(PlayerInput.JUMP):
		body.reset_frame()
		state_machine.set_current_state(get_state(Jumpsquat))
		return true
	return false
	
	
func _enter_state():
	frame = 0


func _exit_state():
	pass
