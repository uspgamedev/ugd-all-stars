extends State
class_name Standing

func _apply_input(input: InputManager):	
	if get_state(Super).try_change_state(input):
		return
	if get_state(Blocking).try_change_state(input):
		return
	if get_state(Attacking).try_change_state(input):
		return
	if get_state(DashStart).try_change_state(input):
		return
	if get_state(Jumpsquat).try_change_state(input):
		return
	if get_state(Walking).try_change_state(input):
		return
	if get_state(Crouching).try_change_state(input):
		return
	if get_state(Air).try_change_state(input):
		return
	

func _update(_delta) -> State:
#	if try_change_state_to(Dashing):
#		set_state(Dashing)
		
	if body.velocity.x != 0:
		body.velocity.x = 0
	if not body.is_on_floor():
		get_state(Air).try_change_state(get_input())
		
#	if try_change_state_to(Jumpsquat):
#		set_state(Jumpsquat)
	return self


func _enter_state():
	body.sprite.play("idle")


func _exit_state():
	pass


func _try_change_state(input: InputManager) -> bool:
	if is_zero_approx(input.get_direction().x):
		state_machine.set_current_state(get_state(Standing))
		return true
	return false
	
