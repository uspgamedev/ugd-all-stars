class_name Shorthop
extends State


func _apply_input(_input: InputManager):
	pass


func _update(_delta):
	body.velocity.y = body.JUMP_VELOCITY
	state_machine.set_current_state(get_state(Air))


func _try_enter_state(_input: InputManager) -> bool:
	state_machine.set_current_state(get_state(Shorthop))
	return true


func _enter_state():
	pass


func _exit_state():
	pass
