extends State
class_name Parrying

const MAX_INPUTS = 10

signal parried(damage_prevented: int)

@onready var hurtbox_manager: HurtBoxManager = Entity.get_entity(self).get_component(HurtBoxManager)
var previous_inputs: Array[InputManager] = []

func parry(damage_prevented: int):
	$ParrykSfx.play()
	parried.emit(damage_prevented)

func _apply_input(_input: InputManager):
	pass
	
func _update(_delta):
	if frame > 10:
		get_state(Normal).change_state()


func _try_change_state(_input: InputManager) -> bool:
	state_machine.set_current_state(get_state(Parrying))
	return true


func _enter_state():
	hurtbox_manager.damage_multiplier = 0.
	hurtbox_manager.knockback = false
	
	
func _exit_state():
	hurtbox_manager.damage_multiplier = 1.0
	hurtbox_manager.knockback = true

