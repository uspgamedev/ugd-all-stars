class_name Air
extends State

const PLATFORM_LAYER = 1 << 5

@export var double_down_combo: Combo
@export var double_down_speed := 0.5
@export var ignore_platform_colision_duration := 1.


func _apply_input(input: InputManager):
	if get_state(Super).try_change_state(input):
		return
	#if get_state(Blocking).try(input):
		#return
	if get_state(AirAttack).try_change_state(input):
		return
	if state_machine.air_jumps > 0:
		#print("posso pular no ar!")
		if get_state(Fullhop).try_change_state(input):
			state_machine.air_jumps -= 1
			return

func ignore_platform():
	body.collision_mask -= (PLATFORM_LAYER)
	await get_tree().create_timer(ignore_platform_colision_duration).timeout
	body.collision_mask += (PLATFORM_LAYER)



func _update(_delta):
	var direction = get_input().get_direction()
	
	# fastfalling
	body.apply_gravity()
	if (
		get_input().or_is_just_pressed([
			DirectionInput.new(PlayerInput.MOVEMENT, DirectionInput.Directions.D),
			DirectionInput.new(PlayerInput.MOVEMENT, DirectionInput.Directions.LD),
			DirectionInput.new(PlayerInput.MOVEMENT, DirectionInput.Directions.RD)
		]) 
		and body.velocity.y > -150 
		and not body.fastfall
	):
		body.velocity.y = body.MAX_FALL_SPEED
		body.fastfall = true
	if body.fastfall == true:
		body.set_collision_mask_value(2, false)
		body.velocity.y = body.MAX_FALL_SPEED
	
	# air drifting 
	if abs(body.velocity.x) >= abs(body.MAX_AIR_HORIZONTAL_SPEED):
		if body.velocity.x > 0:
			if direction.x < 0:
				body.velocity.x += -body.AIR_ACCELERATION
		if body.velocity.x < 0:
			if direction.x > 0:
				body.velocity.x += body.AIR_ACCELERATION				
	# air drifting but 	the character hasn't reached max air speed
	else:
		if direction.x < 0:
			body.velocity.x += -body.AIR_ACCELERATION
		if direction.x > 0:
			body.velocity.x += body.AIR_ACCELERATION	

	# slows down horizontal speed when not pressing buttons
	if not direction.x > 0 and not direction.x < 0:
		if body.velocity.x < 0:
			body.velocity.x += body.AIR_ACCELERATION / 5.
		elif body.velocity.x > 0:
			body.velocity.x += -body.AIR_ACCELERATION / 5.
	
	if body.is_on_floor():
		state_machine.set_current_state(get_state(Landing))

	return self


func _try_change_state(input: InputManager) -> bool:
	if input.check_combo(double_down_combo):
		ignore_platform()
		body.velocity.y = body.gravity*double_down_speed
		
	if body.is_on_floor():
		return false
	state_machine.set_current_state(get_state(Air))
	return true


func _enter_state():
	pass


func _exit_state():
	pass
