class_name AirAttack
extends State


@onready var attack_manager: AttackManager = Entity.get_entity(self).get_component(AttackManager) 


func finish_attack():
	get_state(Normal).change_state()


func _apply_input(_input: InputManager):
	pass


func _update(_delta):
	body.apply_gravity()

	return self

	
func _try_change_state(input: InputManager) -> bool:
	var attack = null
	
	if input.is_pressed(PlayerInput.LIGHT_ATTACK):
		attack = attack_manager.try_attack(0)
	elif input.is_pressed(PlayerInput.HEAVY_ATTACK):
		attack = attack_manager.try_attack(1)
	elif input.is_pressed(PlayerInput.SPECIAL):
		attack = attack_manager.try_attack(1)
	
	if attack:
		state_machine.set_current_state(get_state(AirAttack))
		attack.finished.connect(finish_attack, CONNECT_ONE_SHOT)
		attack.attack()
		return true
	return false


func _enter_state():
	pass


func _exit_state():
	pass
