extends State
class_name Jumping

@onready var body: Body = Entity.get_entity(self).get_component(Body) 

func _update(delta) -> State:
	# problema: só pula se for parado, não se move no ar
	body.velocity.y += ProjectSettings.get_setting("physics/2d/default_gravity") * delta
	if body.is_on_floor():
		return get_state(Standing)
	
	return self

