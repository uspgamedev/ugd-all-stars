extends State
class_name Dashing
	
	
func _apply_input(input: InputManager):
	if get_state(Jumpsquat).try_change_state(input):
		return
	if get_state(Air).try_change_state(input):
		return 
	if get_state(DashAttack).try_change_state(input):
		return
	
	
func _update(_delta):
	var direction = get_input().get_direction()
	
	if not is_zero_approx(direction.x):
		body.velocity.x = direction.x * body.DASHING_SPEED
	
	else:
		body.reset_frame()
		get_state(Normal).change_state()


func _try_change_state(input: InputManager) -> bool:
	var direction = input.get_direction()
	
	if not is_zero_approx(direction.x):
		body.velocity.x = direction.x * body.DASHING_SPEED
		body.turn(direction.x < 0.)
		body.reset_frame()
		state_machine.set_current_state(get_state(Dashing))
		return true
		
	return false


func _enter_state():
	body.sprite.play("walk")


func _exit_state():
	pass
