class_name DashStart
extends State


var dash_start_frames = 3
	

func _apply_input(input: InputManager):
	if get_state(DashStart).try_change_state(input):
		return
	
	
func _update(_delta):
	if get_state(Standing).try_change_state(get_input()):
			return
	if frame >= dash_start_frames:
		if get_state(Dashing).try_change_state(get_input()):
			return
	
	
func _try_change_state(input: InputManager) -> bool:
	var direction := input.get_direction()
	
	if (
		not is_zero_approx(direction.x)
		and not input.is_pressed(PlayerInput.WALK)
	):
		body.velocity.x = direction.x * body.DASHING_SPEED
		body.turn(direction.x < 0.)
		body.reset_frame()
		state_machine.set_current_state(get_state(DashStart))
		return true
	return false
	

func _enter_state():
	frame = 0


func _exit_state():
	pass
