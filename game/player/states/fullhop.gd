class_name Fullhop
extends State


func _apply_input(_input: InputManager):
	pass


func _update(_delta):
	body.velocity.y = body.MAX_JUMP_VELOCITY
	state_machine.set_current_state(get_state(Air))


func _try_change_state(input: InputManager) -> bool:
	if(input.is_pressed(PlayerInput.JUMP)):
		state_machine.set_current_state(get_state(Fullhop))
		return true
	return false


func _enter_state():
	pass


func _exit_state():
	pass
