class_name Normal
extends State


func change_state() -> bool:
	if body.is_on_floor():
		state_machine.set_current_state(get_state(Standing))
	else:
		state_machine.set_current_state(get_state(Air))
		
	return true


func _apply_input(_input: InputManager):
	pass


func _update(_delta) -> State:
	change_state()
	return self


func _try_change_state(_input: InputManager) -> bool:
	state_machine.set_current_state(get_state(Normal))
	return true


func _enter_state():
	pass


func _exit_state():
	pass
