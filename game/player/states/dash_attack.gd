class_name DashAttack
extends State


var dashing_direction: float


@onready var attack_manager: AttackManager = Entity.get_entity(self).get_component(AttackManager)


func try_attack(input: InputManager) -> bool:
	var attack = null
	
	if input.is_pressed(PlayerInput.LIGHT_ATTACK):
		attack = attack_manager.try_attack(1)
	
	if attack:
		state_machine.set_current_state(get_state(DashAttack))
		dashing_direction = get_input().get_direction().x
		attack.finished.connect(finish_attack, CONNECT_ONE_SHOT)
		attack.attack()
		return true
	return false
	

func finish_attack():
	get_state(Normal).change_state()


func _apply_input(_input: InputManager):
	pass


func _update(_delta):
	body.velocity.x = dashing_direction * body.DASHING_SPEED
	return self


func _try_change_state(input: InputManager) -> bool:
	return try_attack(input)
	

func _exit_state():
	pass
