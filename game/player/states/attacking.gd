extends State
class_name Attacking

@onready var attack_manager: AttackManager = Entity.get_entity(self).get_component(AttackManager) 


func _apply_input(_input: InputManager):
	pass


func try_attack(input: InputManager) -> bool:
	var attack = null
	
	if input.is_pressed(PlayerInput.LIGHT_ATTACK):
		attack = attack_manager.try_attack(0)
	#elif input.is_pressed(PlayerInput.HEAVY_ATTACK):
		#attack = attack_manager.try_attack(1)
	elif input.is_pressed(PlayerInput.SPECIAL):
		attack = attack_manager.try_attack(1)
	
	if attack:
		state_machine.set_current_state(self)
		attack.finished.connect(finish_attack, CONNECT_ONE_SHOT)
		attack.attack()
		return true
	return false
	

func finish_attack():
	get_state(Normal).change_state()


func _enter_state():
	pass
	
	
func _try_change_state(input: InputManager) -> bool:
	return try_attack(input)


func _exit_state():
	pass
