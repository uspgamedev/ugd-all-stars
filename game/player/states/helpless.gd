extends State
class_name Helpless

var has_gravity := true

func _apply_input(_input: InputManager):
	pass


func _update(_delta) -> State:
	if has_gravity:
		body.apply_gravity()
	return self


func _try_change_state(_input: InputManager) -> bool:
	return false

func start_helpless(_ignore_damage := true, gravity := true, duration := -1.):
	state_machine.set_current_state(self)
	has_gravity = gravity
	
	if duration > 0.:
		get_tree().create_timer(duration).timeout.connect(finish_helpess)
	
func finish_helpess():
	state_machine.set_current_state(get_state(Normal))

func _enter_state():
	pass


func _exit_state():
	pass
