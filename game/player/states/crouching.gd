class_name Crouching
extends State


func _update(_delta):
	if get_input().get_direction().y >= 0:
		get_state(Normal).change_state()	

	
func _enter_state():
	body.velocity.x = 0
	body.reset_frame()

	
func _exit_state():
	return


func _apply_input(input: InputManager):
	if get_state(Jumpsquat).try_change_state(input):
		return
	if get_state(Air).try_change_state(input):
		return


func _try_change_state(input: InputManager) -> bool:
	if input.get_direction().y < 0:
		state_machine.set_current_state(get_state(Crouching))
		return true
	return false
