extends State
class_name Super

@export var charge := 0 : set = set_charge

@export var super_scene: PackedScene
@export var progress_bar: TextureProgressBar

@export var damage_taken_factor := 2.
@export var damage_dealt_factor := 4.

@onready var charged_animation: AnimatedSprite2D = $SuperProgress/Zap
@onready var entity := Entity.get_entity(self)
@onready var input_manager: InputManager = entity.get_component(InputManager)

func _ready():
	set_charge(charge)

func _update(_delta):
	charge = 0
	var super_instance: SuperAttack = super_scene.instantiate()
	super_instance.activate(entity, input_manager.get_direction())
	state_machine.set_current_state(get_state(Normal))


func _try_change_state(input: InputManager) -> bool:
	if charge >= 100 and input.is_pressed(PlayerInput.LIGHT_ATTACK) and input.is_pressed(PlayerInput.BLOCK):
		state_machine.set_current_state(get_state(Super))
		return true
	return false


func _enter_state():
	pass


func _exit_state():
	pass

func set_charge(charge_: int):
	charge = charge_
	if charge >= 100:
		charge = 100
		if is_inside_tree() and not charged_animation.visible:
			charged_animation.visible = true
			charged_animation.play("default")
	elif is_inside_tree():
		charged_animation.visible = false
		
	progress_bar.value = charge

func on_damage_taken(damage: int):
	charge += int(damage_taken_factor * damage)
	
func on_damage_dealt(damage: int):
	charge += int(damage_dealt_factor * damage)
