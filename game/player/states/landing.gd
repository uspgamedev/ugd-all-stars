class_name Landing
extends State


func _apply_input(_input: InputManager):
	pass


func _update(_delta):
#	if get_parent().state_includes([Air]):
#		if body.is_on_floor():
#			body.frame = 0
#			body.velocity.y = 0
#			body.fastfall = false
#			set_state(Standing) 
	body.frame = 0
	body.velocity.y = 0
	body.fastfall = 0
	state_machine.air_jumps = body.AIR_JUMPS
	state_machine.set_current_state(get_state(Standing))	
	

func _try_change_state(_input: InputManager) -> bool:
	if body.is_on_floor():
		state_machine.set_current_state(get_state(Landing))
		return true
	return false	


func _enter_state():
	pass

	
func _exit_state():
	pass
