extends State
class_name Blocking

@onready var shield: Shield = Entity.get_entity(self).get_component(Shield) 
@onready var hurtbox_manager: HurtBoxManager = Entity.get_entity(self).get_component(HurtBoxManager)


func _apply_input(input: InputManager):
	if not input.is_pressed(PlayerInput.BLOCK):
		shield.unblock()
		if get_state(Parrying).try_change_state(input):
			state_machine.set_current_state(get_state(Parrying))
	
	
func _try_change_state(input: InputManager) -> bool:
	if input.is_pressed(PlayerInput.BLOCK):
		shield.block()
		state_machine.set_current_state(get_state(Blocking))
		return true
	return false
	
	
func _enter_state():
	hurtbox_manager.damage_multiplier = 0.5
	hurtbox_manager.knockback = false
	
	
func _exit_state():
	hurtbox_manager.damage_multiplier = 1.0
	hurtbox_manager.knockback = true
