class_name Walking
extends State


func _apply_input(input: InputManager):
	if get_state(Jumpsquat).try_change_state(input):
		return
	if get_state(Air).try_change_state(input):
		return
	if get_state(Attacking).try_change_state(input):
		return


func _update(_delta):
	if _is_walking(get_input()):
		body.velocity.x = get_input().get_direction().x * body.WALKING_SPEED
	 
	else:
		body.reset_frame()
		get_state(Normal).change_state()


func _try_change_state(input: InputManager) -> bool:
	if _is_walking(input):
		body.velocity.x = input.get_direction().x * body.WALKING_SPEED
		body.turn(input.get_direction().x < 0.)
		body.reset_frame()
		state_machine.set_current_state(get_state(Walking))
		return true
	return false


func _enter_state():
	body.sprite.play("walk")


func _exit_state():
	pass


func _is_walking(input: InputManager):
	return (
		not is_zero_approx(input.get_direction().x)
		and input.is_pressed(PlayerInput.WALK) 
	)
