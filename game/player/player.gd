extends Entity
class_name Player

@export var binding_id := 1
@export var portrait : Texture2D

signal stocks_updated(stocks: int)

@onready var body := $Body

var player_info : Dictionary = {}
var stocks := 3


func _init():
	add_to_group(&"PLAYER_" + str(binding_id))


func _on_body_blastzone_entered(respawn_global_position: Vector2):
	stocks -= 1
	stocks_updated.emit(stocks)
	if stocks:
		body.global_position = respawn_global_position
		body.velocity = Vector2.ZERO
