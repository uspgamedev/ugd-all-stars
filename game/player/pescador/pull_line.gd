extends Node

@export var duration := 0.5

@onready var entity := Entity.get_entity(self)
@onready var state_machine: StateMachine = entity.get_component(StateMachine)
@onready var body: Body = entity.get_component(Body)

func set_hook(hook: Projectile):
	hook.splash.connect(func(_x):
		var helpless: Helpless = state_machine.get_state(Helpless)
		helpless.start_helpless(false, false, duration)
		# TODO: use raycast
		create_tween().tween_property(body, "global_position", hook.global_position-0.3*(hook.global_position-body.global_position), duration)
	
	)

