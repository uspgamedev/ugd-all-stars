extends Area2D
class_name HurtBox

signal hurt(damage: int, knockback: Vector2, stun_duration: float)

@onready var entity := Entity.get_entity(self) 

func _on_area_entered(area: Area2D):
	if (
		area is AttackFrame or
		area is Projectile
	):
		var attack: Attack = area.get_attack()
		
		if Entity.get_entity(attack) != entity: ## and not attack.is_blocked():
			var knockback = attack.get_knockback() 
			knockback.x *= sign(self.global_position.x - attack.global_position.x)
			hurt.emit(attack, knockback)
