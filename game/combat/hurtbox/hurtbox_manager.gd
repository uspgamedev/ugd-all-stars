extends Node2D
class_name HurtBoxManager

@onready var entity := Entity.get_entity(self)
@onready var life: Life = entity.get_component(Life)
@onready var body: Body = entity.get_component(Body)
@onready var state_machine: StateMachine = entity.get_component(StateMachine)

signal hurt(damage: int)

# TODO: refactor to decorator?
var damage_multiplier := 1.0
var knockback = true

func _ready():
	for child in get_children():
		if child is HurtBox:
			child.hurt.connect(_on_hurt)

#func _on_hurt(damage: int, knockback_direction: Vector2, stun_duration: float):
func _on_hurt(attack: Attack, knockback_direction: Vector2):
	if multiplayer.is_server():
		on_hurt.rpc(attack.damage, knockback_direction, attack.stun_duration, attack.get_path())
		
	
	
@rpc("any_peer", "call_local", "reliable")
func on_hurt(damage: int, knockback_direction: Vector2, stun_duration: float, attack_node_path: NodePath):
	var real_damage = int(round(damage*damage_multiplier))
	hurt.emit(real_damage)
	
	if knockback:
		body.apply_knockback(knockback_direction)
		
	
	var attack = get_tree().root.get_node(attack_node_path)
	var state: State = entity.get_component(StateMachine).current_state
	
	if state is Blocking or state is Parrying:
		attack.attack_blocked.emit(entity, real_damage)
	else:
		var helpless: Helpless = state_machine.get_state(Helpless)
		
		attack.attack_hit.emit(entity, real_damage)
		helpless.start_helpless(false, true, stun_duration)
		
		
	
	if state is Parrying:
		state.parry(damage)
	

	
