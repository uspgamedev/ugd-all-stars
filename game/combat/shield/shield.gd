extends Node2D
class_name Shield

const LAYER = 2**7

var blocking := false

func block():
	$Sprite2D.visible = true	
	blocking = true

func unblock():
	$Sprite2D.visible = false
	blocking = false

func is_blocking() -> bool:
	return blocking
