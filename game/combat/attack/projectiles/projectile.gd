class_name Projectile
extends Area2D

@export var projectile_speed: float = 400
@export var direction := Vector2(1., 0.)
@export var lifetime := 1.5
# TODO: refactor to entity and use body
#@export var has_gravity := false

@export var sprite: AnimatedSprite2D

signal hit(target: Entity)
signal splash(projectile: Projectile)
signal missed()

var shooter: Attack

func _ready():
	#$AnimatedSprite2D.play("default")
	get_tree().create_timer(lifetime).timeout.connect(func():
		self.missed.emit()
		self.queue_free()
	)

func _process(delta):
	position += projectile_speed * delta * direction

func shoot(shooter_, direction_, global_pos, rot):
	shooter = shooter_
	direction = direction_
	global_position = global_pos
	global_rotation = rot
	
	if sprite:
		sprite.play("default")
		sprite.flip_h = true
		if rot < -PI/2. or rot > PI/2.:
			sprite.flip_v = true
			sprite.flip_h = true

func get_attack():
	return shooter

var ignore_splash = false

func _on_area_entered(area):
	if (
		area is HurtBox and
		Entity.get_entity(area) != Entity.get_entity(shooter)
	):
		ignore_splash = true
		hit.emit(Entity.get_entity(area))
		queue_free()


func _on_body_entered(body):
	if Entity.get_entity(body) != Entity.get_entity(shooter):
		get_tree().create_timer(0.01).timeout.connect(func(): 
			if not ignore_splash:
				splash.emit(self)
				self.queue_free()
		)
		
