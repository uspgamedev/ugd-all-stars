extends Node2D
class_name Attack

@export var damage := 10
@export var knockback := Vector2(500.0, -300.)
@export var stun_duration := 0.3
@export var recover_time := 0.5

@export var frames := 1

signal finished
signal attack_hit(target: Entity, damage: int)
signal attack_blocked(target: Entity, damage: int)

const PLAYER_LAYER = 1

@onready var entity := Entity.get_entity(self)

var attacking := false

func _notification(what):
	if what == NOTIFICATION_READY:
		self.finished.connect(func(): 
			attacking = false
		)

@rpc("any_peer", "call_local", "reliable")
func attack2():
	attacking = true
	self._attack()
	await self.finished

func attack():
	attack2.rpc()
	
func _attack():
	push_error("Virtual method")

func can_cancel() -> bool:
	push_error("Virtual method")
	return false

func cancel():
	push_error("Virtual method")

func get_damage() -> int:
	return damage

func get_knockback() -> Vector2:
	return knockback

