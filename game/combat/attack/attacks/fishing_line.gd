extends Node2D

@export var k1 := 2.5
@export var k2 := 1.
@export var num_divisions := 25
@export var color := Color.BLACK
@export var width := 4.

@export var L := 500.0

func _process(_delta):
	queue_redraw()

func _draw():
	var line := PackedVector2Array()
	var target = Entity.get_entity(get_parent().shooter).get_component(Body).global_position
	var H = -(target.y - global_position.y)
	var D = -(target.x - global_position.x)
	
	if L**2 < H**2 + D**2:
		return
	
	if abs(D) < L/8:
		return 

	var m = 3/abs(D)*sqrt((L/D)**2-(H/D)**2-1)
	var n = atanh(H/L)-3*sqrt((L/D)**2-(H/D)**2-1)/2.
	
	if  (cosh(n) - 1.)/m > L:
		m = (cosh(n) - 1.)/L
	
	#print(m, " ", n, " ", (cosh(n) - 1.)/m)
	#print("______________________")

	for i in num_divisions+1:
		var x = i/float(num_divisions)
		var x2 = x * abs(D)
		#var x2 = x*L if H < 0 else 1.-x*L
		#var y = -abs(H)*(cosh(k1*x2-k2)-cosh(-k2))/(cosh(k1-k2)-cosh(-k2))
		#var y = -1./m * (2*sinh(m*x2/2. + n)*sinh(m*x2/2.))
		var y = -1./m * (cosh(m*x2+n) - cosh(n))
		if y < -0:
			y = -H
		
		if target.y > global_position.y:
			y = y
		
		line.push_back(Vector2(-D*x, y))
	
	var max_x = 1.
	var max_i = line.size()-1
	for i in range(line.size()-1, 0, -1):
		if abs(line[i].y - line[line.size()-1].y) > 0.1:
			break
		max_i = i
		max_x = -line[i].x/D
	
	for i in line.size():
		line[i].x /= max_x
		
		if abs(line[i].x) > abs(D):
			line[i].x = -D
	
	for i in line.size():
		line[i] =  line[i].rotated(-global_rotation)
	
	draw_polyline(line, color, width)
