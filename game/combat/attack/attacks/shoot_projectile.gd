extends Attack

@export var projectile_scene: PackedScene
@onready var body: Body = Entity.get_entity(self).get_component(Body)
@onready var input_manager: InputManager = Entity.get_entity(self).get_component(InputManager)

signal shoot(projectile: Projectile)

func _attack():
	var direction = input_manager.get_direction()
	var projectile: Projectile = projectile_scene.instantiate()
	projectile.shooter = self
	direction = body.direction * Vector2(1.0, 0.0) if direction.is_zero_approx() else direction
	var global_pos = self.global_position
	var global_rot = -direction.angle_to(Vector2.LEFT)
	
	projectile.shoot(self, direction, global_pos, global_rot)
	World.get_world().add_child(projectile)
	shoot.emit(projectile)
	
	
	await get_tree().create_timer(0.3).timeout
	finished.emit()
