extends Area2D
class_name AttackFrame

@export var frames := 1 

@onready var attack: Attack = get_parent()

func _notification(what):
	if what == NOTIFICATION_READY:
		disable()


func disable():
	switch(true)
		
func enable():
	switch(false)

func switch(disabled: bool):
	for child in get_children():
		child.visible = not disabled
		child.disabled = disabled

func get_attack() -> Attack:
	return attack
