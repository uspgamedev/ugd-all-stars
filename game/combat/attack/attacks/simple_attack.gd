extends Attack
class_name PlayerAttack

const DISABLED := -100

var frame := -DISABLED
var final_frame := 0
var attack_frames = {} # frame -> AttackFrame

func _ready():
	var accumulated_frame = 0
	for child in get_children():
		if child is AttackFrame:
			attack_frames[accumulated_frame] = child
			accumulated_frame += child.frames
			
	final_frame = accumulated_frame

func _attack():
	frame = 0
	change_attack()

#func _update():
func _physics_process(_delta):
	if frame != DISABLED:
		change_attack()
		frame += 1

func change_attack():
	if frame == final_frame:
		frame = DISABLED - 1
		disable_all()
		finished.emit()
		return
		
	if frame in attack_frames:
		disable_all()
		attack_frames[frame].enable()
		

func disable_all():
	for attack_frame in attack_frames.values():
		attack_frame.disable()		
