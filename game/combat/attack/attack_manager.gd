extends Node2D
class_name AttackManager

signal damage_dealt(damage: int)

var attacks : Array[Attack] = []

func _ready():
	for child in get_children():
		if child is Attack:
			attacks.append(child)
			child.attack_hit.connect(func(_e, dam): self.damage_dealt.emit(dam))
			child.attack_blocked.connect(func(_e, dam): self.damage_dealt.emit(dam))

func get_attack(i: int) -> Attack:
	if attacks.size() <= i:
		return null
	return attacks[i]


# is it best by index or name? Like forward_tilt, up_air and so on...
func try_attack(attack_idx: int) -> Attack:
	return get_attack(attack_idx)
