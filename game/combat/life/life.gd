extends Node2D
class_name Life

@export var life := 100

signal life_updated(value: int)

func hurt(damage: int):
	life -= damage
	life_updated.emit(life)
