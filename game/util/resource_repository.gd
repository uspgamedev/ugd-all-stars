@tool
extends Node
class_name ResourceRepository

# TODO: create a script that store all script references: var scripts = {"Resource1": Resource1}


var resources: Dictionary = {} 

func register_resource(resource: Resource, script_name):
	var source_code = "extends RefCounted\nclass_name " + script_name + "\n"
	
	#if Engine.is_editor_hint():
		#var file = FileAccess.open("./resources/", FileAccess.WRITE)
		#file.store_string(source_code)
		
	print(source_code, source_code.md5_text())
	resources[source_code.md5_text()] = resource 
	

func get_resource(script: Script) -> Resource:
	print(script.source_code, script.source_code.md5_text())
	return resources.get(script.source_code.md5_text())
