extends ObjectState

@export var body: Body

@export var position: Vector2

func update_state():
	self.position = body.position

func apply_state():
	body.position = self.position
