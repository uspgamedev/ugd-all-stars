extends CharacterBody2D
class_name Body


@export var life := 100

@export_category("FRAMEDATA")

@export_category("CHARACTER CONSTANTS")
@export var WALKING_SPEED = 150.0
@export var DASHING_SPEED = 300.0
@export var MAX_AIR_HORIZONTAL_SPEED = 300.0
@export var JUMP_VELOCITY = -600.0
@export var MAX_JUMP_VELOCITY = -850.0
@export var FALL_SPEED = 50.0
@export var FALLING_SPEED = 700.0
@export var MAX_FALL_SPEED = 900.0
@export var ACCELERATION = 100.0
@export var AIR_ACCELERATION = 30
@export var AIR_JUMPS = 1

signal blastzone_entered(respawn_global_position: Vector2)

@onready var sprite := $Sprite2D
@onready var input := BodyInput.new()
@onready var current_state := $State

var gravity = ProjectSettings.get_setting("physics/2d/default_gravity")

var frame := 0
 
#var stun := false
var has_double_jump := true
var fastfall := false

var direction = 1

func _physics_process(_delta):
	#handle_input(delta)
	move_and_slide()


func update_frame():
	frame += 1


func reset_frame():
	frame = 0


func apply_knockback(knockback_velocity: Vector2):
	velocity += knockback_velocity
	#stun = true
	#var stun_timer := get_tree().create_timer(stun_duration)
	#await stun_timer.timeout
	#stun = false


func apply_gravity():
	if velocity.y < FALLING_SPEED:
		velocity.y += FALL_SPEED


func turn(dir: bool):
	direction = -1 if dir else 1
	self.transform = Transform2D(0, Vector2(direction, 1), 0., self.position)
