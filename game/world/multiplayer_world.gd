extends World
class_name NetWorld

#@export var character_scenes : Array[PackedScene] = []

func insert_players(room: LobbyRoom):
	var infos = room.get_infos_ordered()
	var points = spawn_points.get_children().map(func(x): return x.position)
	
	var authority_id = room.get_infos_ordered()[0].id
	
	for info in infos:
		var character: Player = character_scenes[info.character_id].instantiate()
		info.is_authority = (info.id == authority_id)
		character.player_info = info
		character.position = points.pop_back()
		character.name = str(info.id)
		character.set_multiplayer_authority(authority_id) 
		players.add_child(character)
