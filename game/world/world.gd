extends Node2D
class_name World

@export var players : Node
@export var spawn_points : Node 
@export var character_scenes : Array[PackedScene] = []

const WORLD_GROUP := "WORLD"

static func get_world() -> World:
	return Engine.get_main_loop().get_first_node_in_group(WORLD_GROUP)

func _notification(what):
	match what:
		NOTIFICATION_READY:
			add_to_group(WORLD_GROUP)

func insert_players(room: LobbyRoom):
	var infos = room.get_infos_ordered()
	var points = spawn_points.get_children().map(func(x): return x.position)
	
	var authority_id = room.get_infos_ordered()[0].id
	
	for info in infos:
		var character: Player = character_scenes[info.character_id].instantiate()
		info.is_authority = (info.id == authority_id)
		character.player_info = info
		character.position = points.pop_back()
		character.name = str(info.id)
		character.set_multiplayer_authority(authority_id) 
		players.add_child(character)


func _input(_event):
	init_joypad()
	
func init_joypad():
	for device in Input.get_connected_joypads():
		var device_info = Input.get_joy_info(device)
		var guid = Input.get_joy_guid(device)
		
		if device_info.get("raw_name", "").to_lower().contains("touchpad"):
			Input.add_joy_mapping(guid + ",Touchpad,platform:Linux", true)
