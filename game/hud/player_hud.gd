extends CenterContainer

@onready var life_label := $"%LifeLabel"
@onready var portrait := $"%Portrait"
@onready var stocks_container := $"%StocksContainer"


func setup(player: Player):
	player.stocks_updated.connect(_on_stocks_updated)
	var player_life: Life = player.get_component(Life)
	portrait.texture = player.portrait
	_on_life_updated(player_life.life)
	player_life.life_updated.connect(_on_life_updated)


func _on_life_updated(life: int):
	life_label.text = "%d" % life


func _on_stocks_updated(stocks: int):
	for i in stocks_container.get_child_count():
		(stocks_container.get_child(i) as TextureRect).visible = i < stocks
