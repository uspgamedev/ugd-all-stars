extends CanvasLayer

@export var player_hud_scene: PackedScene

@onready var player_hud_container := $"%PlayerHUDContainer"


func _ready():
	for player in get_tree().get_nodes_in_group("PLAYER"):
		var player_hud = player_hud_scene.instantiate()
		player_hud_container.add_child(player_hud)
		player_hud.setup(player)
