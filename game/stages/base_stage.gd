extends Node2D

@onready var respawn := $Respawn


func _on_safezone_body_exited(body):
	if body is Body:
		body.blastzone_entered.emit(respawn.global_position)
