extends Camera2D


@onready var player_1 = get_tree().get_first_node_in_group("PLAYER_1")
@onready var player_2 = get_tree().get_first_node_in_group("PLAYER_2")


func _process(_delta):
	if player_1 and player_2:
		global_position = (player_1.body.global_position + player_2.body.global_position)/2
	elif player_1:
		global_position = player_1.body.global_position
	elif player_2:
		global_position = player_2.body.global_position
