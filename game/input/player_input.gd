extends InputType
class_name PlayerInput

static var LIGHT_ATTACK = BoolInput.new("light_attack")
static var HEAVY_ATTACK = BoolInput.new("heavy_attack")
static var JUMP = BoolInput.new("jump")
static var SPECIAL = BoolInput.new("special")
static var BLOCK = BoolInput.new("block")
static var WALK = BoolInput.new("walk")
#static var CROUNCH = BoolInput.new("crouch")
static var MOVEMENT = DirectionalInput.new("movement")


func _get_bool_inputs() -> Array[BoolInput]:
	return [
		LIGHT_ATTACK,
		HEAVY_ATTACK,
		JUMP,
		SPECIAL,
		BLOCK,
		WALK,
		#CROUNCH,
	]
	
func _get_directional_inputs() -> Array[DirectionalInput]:
	return [
		MOVEMENT
	]

	
