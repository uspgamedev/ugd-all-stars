extends Node

var server := false

@export var lobby_api_scene: PackedScene

@export_file("*.tscn") var server_scene_path := ""
@export var config: LobbyConfig
@export var debug_config: LobbyConfig

@export var world_scene: PackedScene
@export var entry_scene: PackedScene

@export var join_button: Button
@export var ready_button: Button
@export var room_name_label: Label
@export var room_name_field: TextEdit
@export var entries: RichTextLabel
@export var name_field: TextEdit
@export var color1_field: ColorPickerButton
@export var color2_field: ColorPickerButton
@export var mode_picker: CheckButton

# TODO: automatic populate selection
@onready var character_selection: OptionButton = %CharacterSelection

var lobby_api: LobbyAPI = null

var joined := false
var server_pid = null

func _ready():
	lobby_api = lobby_api_scene.instantiate()
	get_tree().root.add_child.bind(lobby_api).call_deferred()
	lobby_api.room_updated.connect(update_room_info)
	lobby_api.game_started.connect(start_game)

func create_server():
	close_server()
	server_pid = OS.create_instance([server_scene_path])

func close_server():
	if server_pid != null: 
		OS.kill(server_pid)
		server_pid = null

func get_player_info() -> Dictionary:
	return {
		"name": name_field.text,
		"character_id": character_selection.get_selected_id()
	}

func update_room_info(room: LobbyRoom):
	room_name_label.text = room.room_name + " "
	entries.text = ""
	for info in room.player_infos.values():
		entries.text += "[color=%s] %s[/color]\n" % [Color.GREEN.to_html() if info.ready else Color.RED.to_html(), info.name]
	
func start_game(room: LobbyRoom):
	var world: World = world_scene.instantiate()
	world.ready.connect(func(): world.insert_players(room))
	add_sibling(world)
	queue_free()


func enter_room():
	lobby_api.config = config if mode_picker.button_pressed else debug_config
	lobby_api.enter_room(room_name_field.text, get_player_info())

func _on_join_room_pressed():
	if not joined:
		room_name_field.editable = false
		join_button.text = "Leave room"
		enter_room()
		joined = true
	else:
		joined = false
		join_button.text = "Join room"
		room_name_field.editable = true
		lobby_api.leave_room()
		%RoomName.text = "Room"
		for entry in %Entries.get_children():
			entry.queue_free()
		

func _on_ready_pressed():
	if joined:
		ready_button.disabled = true
		lobby_api.set_ready(true)


func _on_server_pressed():
	create_server()

